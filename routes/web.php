<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//
//    return view('welcome');
//})->name('welcome');

Route::get('/','FrontendController@index')->name('welcome');

//Route::get('/products', function () {
//
//return view('frontend.products');
//})->name('products');

Route::get('/products/{category}/{subCategory?}', 'FrontendController@products')->name('products.list');

//Route::get('/product-details', function () {
//
//    return view('frontend.product-details');
//})->name('product_details');

Route::get('/product-details/{product}', 'FrontendController@productDetails')->name('product.details');




Route::get('/checkout', function () {

    return view('frontend.checkout');
})->name('products.checkout');



Route::post('/add_to_cart', 'CartController@addToCart')->name('add_to_cart');

Route::get('/shopping-bag', 'CartController@getProductsFromCart')->name('shopping_bag');
Route::post('/remove_From_cart', 'CartController@removeFromCart')->name('remove_From_cart');








Auth::routes(['register' => true]);

//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['middleware'=>['auth'], 'prefix'=>'admin'], function () {

    Route::get('/dashboard', function () {
        return view('backend.dashboard');
    })->name('admin.index');

    Route::get('/ad/login', function () {
        return view('backend.login');
    })->name('admin.login');

    Route::get('/ad/register', function () {
        return view('backend.register');
    })->name('admin.register');

    Route::get('/admin/forgot-password', function () {
        return view('backend.forgot-password');
    })->name('forgot-password');

    Route::get('/404', function () {
        return view('backend.404');
    })->name('404');

    Route::get('/blank', function () {
        return view('backend.blank');
    })->name('blank');

    Route::get('/charts', function () {
        return view('backend.charts');
    })->name('charts');

    Route::get('/tables', function () {
        return view('backend.tables');
    })->name('tables');

    Route::get('/buttons', function () {
        return view('backend.buttons');
    })->name('buttons');

    Route::get('/cards', function () {
        return view('backend.cards');
    })->name('cards');

    Route::get('/utilities-color', function () {
        return view('backend.utilities-color');
    })->name('utilities-color');

    Route::get('/utilities-border', function () {
        return view('backend.utilities-border');
    })->name('utilities-border');

    Route::get('/utilities-animation', function () {
        return view('backend.utilities-animation');
    })->name('utilities-animation');

    Route::get('/utilities-other', function () {
        return view('backend.utilities-other');
    })->name('utilities-other');




Route::get('/my-profile', 'UserController@myProfile')->name('my_profile');
Route::get('/users/{id}/edit', 'UserController@edit')->name('users.edit');
Route::put('/users/{id}', 'UserController@update')->name('users.update');

//Route::get('/categories', 'CategoryController@index')->name('categories.index');
//
//Route::get('/categories/create', 'CategoryController@create')->name('categories.create');
//
//Route::post('/categories', 'CategoryController@store')->name('categories.store');
//
//Route::get('/categories/{category}', 'CategoryController@show')->name('categories.show');
//
//Route::get('/categories/{category}/edit', 'CategoryController@edit' )->name('categories.edit');
//
//Route::put('/categories/{category}', 'CategoryController@update')->name('categories.update');
//
//Route::delete('/categories/{category}', 'CategoryController@destroy')->name('categories.destroy');
    Route::resource('categories', 'CategoryController');
    Route::resource('sub-categories', 'SubCategoryController');





    Route::get('/products/pdf', 'PdfController@products')->name('products.pdf');
    Route::get('/products/excel', 'ExcelController@products')->name('products.excel');




    Route::get('/products/trash', 'TrashController@trash')->name('products.trash');
    Route::get('/products/trash/{id}', 'TrashController@showTrash')->name('products.trash_show');
    Route::put('/products/trash/{id}', 'TrashController@restoreTrash')->name('products.trash_restore');
    Route::delete('/products/trash/{id}', 'TrashController@deleteTrash')->name('products.trash_delete');


//Route::get('/products', 'ProductController@index')->name('products.index');
//
//Route::get('/products/create', 'ProductController@create')->name('products.create');
//
//Route::post('/products', 'ProductController@store')->name('products.store');
//
//Route::get('/products/{product}', 'ProductController@show')->name('products.show');
//
//Route::get('/products/{product}/edit', 'ProductController@edit' )->name('products.edit');
//
//Route::put('/products/{product}', 'ProductController@update')->name('products.update');
//
//Route::delete('/products/{product}', 'ProductController@destroy')->name('products.destroy');

    Route::resource('products', 'ProductController');
//    Route::get('color/test', 'ColorController@test');
    Route::resource('colors', 'ColorController');
    Route::resource('sizes', 'SizeController');
    Route::resource('brands', 'BrandController');
    Route::resource('tags', 'TagController');
    Route::resource('blogs', 'BlogController');
    Route::get('products/{product}/{notification}', 'ProductController@show')->name('products.notification');
    Route::post('products/{product}/comment', 'CommentController@productComment')->name('products.comment');

});

//Route::post('products/{product}/comment', 'CommentController@productComment')->name('products.comment');







//Auth::routes();
//
//Route::get('/home', 'HomeController@index')->name('home');
