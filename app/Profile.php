<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = ['user_id', 'facebook_url', 'twitter_url', 'linked_in_url', 'picture', 'bio'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
