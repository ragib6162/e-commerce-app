<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    const ADMIN = 2;
    const EDITOR = 1;
    const GUEST = 10;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'country_id', 'role_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
   public function profile()
   {
       return $this->hasOne(Profile::class, 'user_id', 'id');
   }
   public function blogs()
   {
       return $this->hasMany(Blog::class);
   }
   public function role()
   {
       return $this->belongsTo(Role::class);
   }
   public function isAdmin()
   {
       return $this->role->id === self::ADMIN;
   }
    public function isEDITOR()
    {
        return $this->role->id === self::EDITOR;
    }
    public function isGUEST()
    {
        return $this->role->id === self::GUEST;
    }

}
