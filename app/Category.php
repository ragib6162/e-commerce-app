<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
//   protected $table='categories1';
//
//    public $timestamps=false;
      protected $fillable =['title', 'description','is_active'];

      public function products()
      {
          return $this->hasMany(Product::class, 'category_id', 'id');
      }
      public function subCategories()
      {
          return $this->hasMany(SubCategory::class);
      }

}
