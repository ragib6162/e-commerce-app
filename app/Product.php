<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable =['product_title', 'description','is_active','category_id', 'sub_category_id',  'image', 'created_by', 'updated_by'];

     public function category()
     {
         return $this->belongsTo(Category::class);
     }
    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }
     public function createdBy()
     {
         return $this->belongsTo(User::class, 'created_by');
     }
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
    public function colors()
    {
        return $this->belongsToMany(Color::class);
    }
    public function tags()
    {
        return $this->morphToMany(Tag::class , 'taggables');
    }
    public function blogs()
    {
        return $this->morphToMany(Blog::class,'taggables');
    }
    public function sizes()
    {
        return $this->belongsToMany(Size::class);
    }
    public function brands()
    {
        return $this->belongsToMany(Brand::class);
    }
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
    public function commentedBy()
    {
        return $this->belongsTo(User::class, 'commented_by');
    }
    public function pictures()
    {
        return $this->morphMany(Image::class, 'imagable');
    }

}

