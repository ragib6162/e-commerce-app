<?php

namespace App\Http\Controllers;

use App\Tag;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $tags =Tag::orderBy('created_at','desc')->paginate(10);

        return view('backend.tags.index', compact('tags','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $users = User::pluck('name','id')->toArray();

        return view('backend.tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $tags = $request->all();
//        dd($colors);
        try {
            $tags['created_by']=auth()->user()->id;

            Tag::create($tags);

            return redirect()->route('tags.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Tag $tag) //route model model binding/ dependency injection
    {

        return view('backend.tags.show', compact('tag'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Tag $tag)
    {
//        $users = User::pluck('name','id')->toArray();

        return view('backend.tags.edit', compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tag $tag)
    {
        try{
            $request['updated_by']=auth()->user()->id;
            $tag->update($request->all());
            return redirect()->route('tags.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tag $tag)
    {
        try{
            $tag->delete();
            return redirect()->route('tags.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
