<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$categories = DB :: table('categories')->get();
        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
//        dd($sl);
        $categories =Category::orderBy('created_at','desc')->paginate(10);

        //dd($categories);

        return view('backend.categories.index', compact('categories','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
//        dd($_POST);
        try {

            Category::create($request->all());

            return redirect()->route('categories.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category) //route model model binding/ dependency injection
    {
//        dd($category->products);
//        $category=Category::findOrFail($category);
//        dd($category);
        return view('backend.categories.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
//        $category=Category::findOrFail($id);
//       dd($category);
        return view('backend.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        try{
//                $category=Category::findOrFail($id);
        $category->update($request->all());
//        dd($category);
        return redirect()->route('categories.index')->withStatus('Updated successfully!');
    }catch (QueryException $e){
        return redirect()->back()->withErrors($e->getMessage());
    }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        try{
//            $category=Category::findOrFail($id);
            $category->delete();
//        dd($category);
            return redirect()->route('categories.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
