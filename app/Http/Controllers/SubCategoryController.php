<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;

use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class SubCategoryController extends Controller
{
    public function index()
    {


        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $subCategories= SubCategory::orderBy('created_at', 'desc')->get();

        return view('backend.sub-categories.index', compact('subCategories', 'sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title','id');
        return view('backend.sub-categories.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
//            $request->validate(
//                ['title' => ['required','min:3','max:5']],
//                ['title.min' => 'this is my custom error message for minimum character']
//            );

//            $validator = Validator::make($request->all(), [
//                'title' => 'required|min:10',
//            ]);
//
//            if ($validator->fails()) {
//                return 'Validation failed';
////                return redirect('post/create')
////                    ->withErrors($validator)
////                    ->withInput();
//            }

            $data = $request->all();
//            dd($data);
            $data['created_by'] = auth()->user()->id;

            if ($request->hasFile('image')) {
                $data['image'] = $this->uploadImage($request->image, $request->title);
            }
            SubCategory::create($data);

//        session(['status' => 'Created Successfully !']);

//        session()->flash('status', 'Created Successfully !');

            return redirect()->route('sub-categories.index')->withStatus('Created Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory) //route model model binding/ dependency injection
    {
////        $SubCategory = SubCategory::find(3000);
////        $SubCategory = SubCategory::findOrFail($id);
//
////        $SubCategory = SubCategory::where('id', 3000)->first();
//
////        $SubCategory = SubCategory::where('id', 3000)->get();
//
////        dd($SubCategory);
        return view('backend.sub-categories.show', compact('subCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
//        $SubCategory = SubCategory::findOrFail($id);
        $categories = Category::pluck('title','id');

        return view('backend.sub-categories.edit', compact('subCategory','categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        try{
//            $SubCategory = SubCategory::findOrFail($id);
            $data = $request->all();
            $data['updated_by'] = auth()->user()->id;
            if ($request->hasFile('image')) {
                $this->unlink($subCategory->image);
                $data['image'] = $this->uploadImage($request->image, $request->title);
            }
            $subCategory->update($data);
            return redirect()->route('sub-categories.index')->withStatus('Updated Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        try{
//            $SubCategory = SubCategory::findOrFail($id);
            $subCategory->delete();
            return redirect()->route('sub-categories.index')->withStatus('Deleted Successfully !');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    private function uploadImage($file, $name)
    {
//        dd($file);
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();

        $pathToUpload = storage_path().'/app/public/sub-categories/';

        Image::make($file)->resize(100,100)->save($pathToUpload.$file_name);
//        dd($file_name);
        return $file_name;
    }

    private function unlink($file)
    {
//        dd($file);
        $pathToUpload = storage_path().'/app/public/sub-categories/';
        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }
}
