<?php

namespace App\Http\Controllers;

use App\Size;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $sizes =Size::orderBy('created_at','desc')->paginate(10);

        return view('backend.sizes.index', compact('sizes','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.sizes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $sizes = $request->all();
        try {
//            $colors['created_by']=auth()->user()->id;
            Size::create($sizes);

            return redirect()->route('sizes.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Size $size) //route model model binding/ dependency injection
    {

        return view('backend.sizes.show', compact('size'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Size $size)
    {

        return view('backend.colors.edit', compact('size'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Size $size)
    {
        try{
//            $request['updated_by']=auth()->user()->id;

            $size->update($request->all());
            return redirect()->route('sizes.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Size $size)
    {
        try{
            $size->delete();
            return redirect()->route('sizes.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
