<?php

namespace App\Http\Controllers;

use App\Profile;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
//use Intervention\Image\Image;

class UserController extends Controller
{
    public function myProfile()
    {
        $user = auth()->user();

//        dd($user);
        return view('backend.users.profile', compact('user'));
    }
    public function edit()
    {
        $user = auth()->user();
        return view('backend.users.edit', compact('user'));
    }
    public function update(Request $request)
    {
//        dd($request->all());
        $user = auth()->user();
        $userData = $request->only('name', 'email');
        $profileData = $request->only('facebook_url', 'twitter_url', 'linked_in_url','picture', 'bio');

//        $this->uploadImage("something");
//        dd($request->hasFile('picture'));

        if ($request->hasFile('picture')){
            $this->unlink($user->profile->picture);
            $profileData['picture'] = $this->uploadImage($request->picture, $user->name);
        }
//        dd($profileData);

//       dd($user->profile->picture);

        $user->update($userData);
        $user->profile()->update($profileData);

        return redirect()->route('my_profile')->withStatus('Updated successfully!');
    }

    private function uploadImage($file, $name)
    {
//        dd($file);
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();
        $pathToUpload = storage_path().'/app/public/users/';
        Image::make($file)->resize(100,100)->save($pathToUpload.$file_name);
        return $file_name;
    }
//    public function updateImage($some)
//    {
//        dd($some);
//    }
    private function unlink($file)
    {
//       dd($file);
        $pathToUpload = storage_path().'/app/public/users/';

        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }



}
