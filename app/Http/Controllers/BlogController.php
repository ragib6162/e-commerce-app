<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Tag;
use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $this->authorize('blogs.viewAny');

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $blogs =Blog::orderBy('created_at','desc')->paginate(10);

        return view('backend.blogs.index', compact('blogs','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $users = User::pluck('name','id')->toArray();
        $tags = Tag:: pluck('name','id')->toArray();
        $selectedTags=[];

        return view('backend.blogs.create',compact('tags','selectedTags'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $blogs = $request->all();
        try {
            $blogs['created_by']=auth()->user()->id;
            $tags=$request->tag;
            $blog['created_by']= auth()->user()->id;
//            dd($tags);

            $blog=Blog::create($blogs);
            $blog->tags()->attach($tags);

            return redirect()->route('blogs.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog) //route model model binding/ dependency injection
    {

        return view('backend.blogs.show', compact('blog'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Blog $blog)
    {
//        $this->authorize('update-blog', $blog);
//        $blog->tags()->pluck('id');
//        dd($blog->tags()->pluck('id'));
//        $users = User::pluck('name','id')->toArray();
        $tags = Tag:: pluck('name','id')->toArray();
        $selectedTags= $blog->tags->pluck('id')->toArray();
//        dd($selectedTags);


        return view('backend.blogs.edit', compact('blog','tags','selectedTags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
//        dd($blog);
        try{
            $request['updated_by']=auth()->user()->id;
            $tags=$request->tag;
            $blog->update($request->all());
            $blog->tags()->sync($tags);
            return redirect()->route('blogs.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        try{
            $blog->delete();
            $blog->tags()->detach();
            return redirect()->route('blogs.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
