<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class TrashController extends Controller
{
    public function trash()
    {
//        dd("Hello");

        $sl = !is_null(\request()->page) ? (\request()->page -1 )* 10 : 0;

        $products = Product::onlyTrashed()->paginate(10);
        return view('backend.products.trash', compact('products', 'sl'));
    }
    public function showTrash($id)
    {
//        dd($id);
        $product = Product::onlyTrashed()->where('id', $id)->first();

        return view('backend.products.show', compact('product'));
    }
    public function restoreTrash($id)
    {
        try {
            $product = Product::onlyTrashed()->where('id', $id)->first();
            $product->restore();
            return redirect()->route('products.trash')->withStatus('Updated Successfully');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function deleteTrash($id)
    {
        try {
            $product = Product::onlyTrashed()->where('id', $id)->first();
            $product->forceDelete();
            return redirect()->route('products.trash')->withStatus('Deleted Successfully');
        }catch (QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
}
