<?php

namespace App\Http\Controllers;

use App\Brand;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Image;

class BrandController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $brands =Brand::orderBy('created_at','desc')->paginate(10);

        return view('backend.brands.index', compact('brands','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('backend.brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());

        try {
            $data = $request->all();
            $data['created_by'] = auth()->user()->id;
            if ($request->hasFile('image')){

                $data['image'] = $this->uploadImage($request->image, $request->name);
            }


//            dd($brands);
            Brand::create($data);

            return redirect()->route('brands.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand) //route model model binding/ dependency injection
    {

        return view('backend.brands.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {

        return view('backend.brands.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        try{
            $data = $request->all();
            $data['updated_by']=auth()->user()->id;
//            dd($request->all());
            if ($request->hasFile('image')){
                $this->unlink($brand->image);
                $data['image'] = $this->uploadImage($request->image, $request->name);
            }

            $brand->update($data);
            return redirect()->route('brands.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        try{
            $brand->delete();
            return redirect()->route('brands.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    private function uploadImage($file, $name)
    {
//        dd($file);
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();
//        dd($file_name);
        $pathToUpload = storage_path().'/app/public/brands/';
        Image::make($file)->resize(100,100)->save($pathToUpload.$file_name);
//        dd(($pathToUpload.$file_name));
        return $file_name;
    }
    private function unlink($file)
    {
//       dd($file);
        $pathToUpload = storage_path().'/app/public/brands/';

        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }
}
