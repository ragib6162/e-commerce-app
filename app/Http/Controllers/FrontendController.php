<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use App\SubCategory;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function products(Category $category, SubCategory $subCategory)
    {
          $products = $subCategory->products;
          $products = Product::where('sub_category_id', $subCategory->id)->get();
//          dd($products);
          return view('frontend.products', compact('products'));
    }
    public function productDetails(Product $product)
    {
        return view('frontend.product-details', compact('product'));
    }
}
