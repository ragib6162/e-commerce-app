<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Product;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function products()
    {
        return Excel::download(new ProductsExport(), 'products.xlsx');
    }
}
