<?php

namespace App\Http\Controllers;


use App\Color;

use App\User;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {

        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
        $colors =Color::orderBy('created_at','desc')->paginate(10);

        return view('backend.colors.index', compact('colors','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $users = User::pluck('name','id')->toArray();

        return view('backend.colors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $colors = $request->all();
//        dd($colors);
        try {
            $colors['created_by']=auth()->user()->id;
            Color::create($colors);

            return redirect()->route('colors.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Color $color) //route model model binding/ dependency injection
    {

        return view('backend.colors.show', compact('color'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Color $color)
    {
//        $users = User::pluck('name','id')->toArray();

        return view('backend.colors.edit', compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Color $color)
    {
        try{
            $request['updated_by']=auth()->user()->id;

            $color->update($request->all());
            return redirect()->route('colors.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Color $color)
    {
        try{
            $color->delete();
            return redirect()->route('colors.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }
}
