<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Color;
use App\Product;
use App\Http\Requests;
use App\Size;
use App\SubCategory;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\ProductRequest;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$categories = DB :: table('categories')->get();
        $sl = !is_null(\request()->page) ? (\request()->page-1) * 10 : 0;
//        dd($sl);
        $products =Product::with('category', 'subCategory', 'createdBy')->orderBy('created_at','desc')->paginate(10);

//        dd($products);

        return view('backend.products.index', compact('products','sl'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $subCategories = SubCategory::pluck('title', 'id')->toArray();
//        $users = User::pluck('name', 'id')->toArray();
        $colors = Color::pluck('name', 'id')->toArray();
        $selectedColors = [];
        $tags = Tag::pluck('name', 'id')->toArray();
        $selectedTags = [];
        $sizes = Size::pluck('name', 'id')->toArray();
        $selectedSizes = [];
        $brands = Brand::pluck('name', 'id')->toArray();
        $selectedBrands = [];
//        dd($categories);
        return view('backend.products.create', compact('categories', 'subCategories', 'colors','selectedColors','tags','selectedTags', 'sizes', 'selectedSizes', 'brands', 'selectedBrands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        $products = $request->all();
//                dd($products);

        try {
            $products['created_by']=auth()->user()->id;
            if ($request->hasFile('image')){

                $products['image'] = $this->uploadImage($request->image, $request->product_title);
            }

//            $validated = $request->validated();
            $colors = $request->color;
            $tags = $request->tag;
            $sizes = $request->size;
            $brands = $request->brand;

            $product = Product::create($products);

            if($request->hasFile('picture')){
                foreach ($request->picture as $pic){
                    $product->pictures()->create([
                        'picture' => $this->uploadMultipleImage($pic),
                    ]);
                }
            }
            $product->colors()->attach($colors);
            $product->tags()->attach($tags);
            $product->sizes()->attach($sizes);
            $product->brands()->attach($brands);

            return redirect()->route('products.index')->withStatus('Created successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product, $notificationId='')
    {
//        dd($product);
        $this->authorize('products.view', $product);

        if(!is_null($notificationId)){
            DB::table('notifications')
                ->where('id', $notificationId)
                ->update(['read_at'=>Carbon::now()]);
        }


        return view('backend.products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::pluck('title', 'id')->toArray();
        $subCategories = SubCategory::pluck('title', 'id')->toArray();
//        $users = User::pluck('name', 'id')->toArray();
        $colors = Color::pluck('name', 'id')->toArray();
        $selectedColors = $product->colors->pluck('id')->toArray();
        $tags = Tag::pluck('name', 'id')->toArray();
        $selectedTags= $product->tags->pluck('id')->toarray();
        $sizes = Size::pluck('name', 'id')->toArray();
        $selectedSizes = $product->sizes->pluck('id')->toarray();
        $brands = Brand::pluck('name', 'id')->toArray();
        $selectedBrands = $product->brands->pluck('id')->toarray();
//        $product=Product::findOrFail($product);
//       dd($category);
        return view('backend.products.edit', compact('product','categories', 'subCategories','colors','selectedColors','tags','selectedTags','sizes', 'selectedSizes', 'brands', 'selectedBrands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $products = $request->all();
//        dd($products);
        try{
            $products['updated_by']=auth()->user()->id;
            if ($request->hasFile('image')){
                $this->unlink($product->image);
                $products['image'] = $this->uploadImage($request->image, $request->product_title);
            }

            if($request->hasFile('picture')){
                foreach ($request->picture as $pic){
                    $product->pictures()->update([
                        'picture' => $this->uploadMultipleImage($pic),
                    ]);
                }
            }

//                $category=Category::findOrFail($id);
            $product->update($products);
            $colors = $request->color;
            $tags = $request->tag;
            $sizes = $request->size;
            $brands = $request->brand;

            $product->colors()->sync($colors);
            $product->tags()->sync($tags);
            $product->sizes()->sync($sizes);
            $product->brands()->sync($brands);
//        dd($category);
            return redirect()->route('products.index')->withStatus('Updated successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
//       dd($product);
        try{
//            $category=Category::findOrFail($id);
            $product->colors()->detach();
            $product->tags()->detach();
            $product->sizes()->detach();
            $product->brands()->detach();

            $product->delete();
//        dd($category);
            return redirect()->route('products.index')->withStatus('Delete successfully!');
        }catch (QueryException $e){
            return redirect()->back()->withErrors($e->getMessage());
        }
    }



    private function uploadImage($file, $name)
    {
//        dd($file);
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());//formatting the name for unique and readable
        $file_name = $timestamp .'-'.$name. '.' . $file->getClientOriginalExtension();
        $pathToUpload = storage_path().'/app/public/products/';
        Image::make($file)->resize(200,200)->save($pathToUpload.$file_name);
        return $file_name;
    }
    private function unlink($file)
    {
//       dd($file);
        $pathToUpload = storage_path().'/app/public/products/';

        if ($file != '' && file_exists($pathToUpload. $file)) {
            @unlink($pathToUpload. $file);
        }
    }

    private function uploadMultipleImage($file)
    {
        $timestamp = str_replace([' ', ':'], '-', Carbon::now()->toDateTimeString());
        $name = $file->getClientOriginalName();
        $file_name = $timestamp .'-' . $name;
        $pathToUpload = storage_path().'/app/public/products/';
        Image::make($file)->resize(80,100)->save($pathToUpload.$file_name);
        return $file_name;
    }
}


