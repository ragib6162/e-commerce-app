<?php


namespace App\Http\View\Composers;

use App\Product;
use Illuminate\Support\Facades\Cookie;
use Illuminate\View\View;
use App\Category;

class FrontendComposer
{
    public function compose(View $view)
    {
        $categories = Category::with('subCategories')->orderBy('created_at','desc')->get();
//        $products = Product::orderBy('created_at','desc')->get();
        $productIds = Cookie::get('productIds');

        if(!is_null($productIds)){
            $productIds = unserialize($productIds);
        }
        else{
            $productIds = [];
        }
//     dd($productIds);
        $view->with(compact('categories',  'productIds'));
    }
}
