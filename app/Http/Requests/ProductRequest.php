<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
//        'unique:products'
        return [
            'product_title' => ['required','min:3','max:20', ],
            'description' => ['required','min:10','max:500'],
        ];
    }
    public function messages()
    {
        return [
            'product_title.required' => 'A product_title is required',
            'product_title.min' => 'Minimum test',
            'description.required' => 'Description is required',
        ];
    }

}
