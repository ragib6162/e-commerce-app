<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['title', 'description', 'created_by', 'updated_by'];

    public function products()
    {
        return $this->morphedByMany(Product::class,'taggables');
    }
    public function tags()
    {
        return $this->morphToMany(Tag::class , 'taggables', 'taggables');
    }
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }



}
