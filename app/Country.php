<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable =['name','created_by'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function blogs()
    {
        return $this->hasManyThrough(
            'App\Blog',
            'App\User',
            'country_id', // Foreign key on users table...
            'created_by', // Foreign key on blogs table...
            'id', // Local key on countries table...
            'id' // Local key on users table...
        );
    }
}
