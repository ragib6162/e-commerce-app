<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable=['name', 'created_by', 'updated_by'];
    public function products()
    {
        return $this->morphedByMany(Product::class, 'taggables');
    }
    public function blogs()
    {
        return $this->morphedByMany(Blog::class,'taggables');
    }
    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
