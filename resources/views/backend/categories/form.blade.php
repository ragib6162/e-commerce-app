

<div class="form-row">
                    <div class="col-md-12 mb-3">
{{--                        <label for="title">Title</label>--}}
                        {{Form::label('title','Title')}}
{{--                        <input type="text" name="title" class="form-control " placeholder="Enter The Title" >--}}
                        {{Form::text('title', null, [
                              'class'=> $errors->has('title') ?  'form-control is-invalid': 'form-control',
                              'placeholder'=>'Enter The Title',
                        ])}}
                        @error('title')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror

                    </div>

                </div>

                <div class="form-row">
                    <div class="col-md-12 mb-3">
{{--                        <label for="validationServer01">Title</label>--}}
{{--         <input type="radio" name="is_active" value="{{True}}">Active--}}
                        {{Form::label('is_active','Active')}}
                        {{Form::radio('is_active','1')}}
{{--         <input type="radio" name="is_active" value="{{False}}">Inactive--}}
                        {{Form::label('is_active','Inactive')}}
                        {{Form::radio('is_active','0')}}

            </div>
      </div>
{{--<div class="form-row">--}}
{{--    <div class="col-md-12 mb-3">--}}
{{--        <select name="category_id">--}}
{{--            <option value="Men's Category">Men's Category</option>--}}
{{--            <option value="Women's Category">Women's Category</option>--}}
{{--            <option value="Kid's Category">Kid's Category</option>--}}
{{--        </select>--}}
{{--             {{Form::select('category_id', [--}}
{{--                    "Men's Category"=>"Men's Category",--}}
{{--                    "Women's Category"=>"Women's Category",--}}
{{--                    "Kid's Category"=>"Kid's Category",--}}

{{--              ], null, ["placeholder"=>"Men's Category"])}}--}}

{{--    </div>--}}
{{--</div>--}}

<div class="form-row">
    <div class="col-md-12 mb-3">
{{--        <label for="description">Description</label>--}}
           {{Form::label('description','Description')}}
{{--        <textarea type="text" name="description" class="form-control " placeholder="Enter The Description" >--}}
{{--                        </textarea>--}}
        {{Form::textarea('description', null, [
                          'class'=>$errors->has('description') ?  'form-control is-invalid': 'form-control',
                          'placeholder'=>'Enter The Description',
                          'rows'=>'2',
                    ])}}

    </div>
    @error('description')
    <div class="text-danger">{{ $message }}</div>
    @enderror
</div>
