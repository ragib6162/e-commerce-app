@extends('backend.layouts.master')

@section('title', 'Category Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show Category</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('categories.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Title</th>
                        <th>{{ $category->title }}</th>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <th>{{ $category->description }}</th>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>{{ $category->is_active ? 'Active' : 'Inactive' }}</th>
                    </tr>
                    <tr>
                        <th>Products</th>
                        <th>
                            <ul>
                            @foreach($category->products as $product)
                                <li>{{$product->product_title}}</li>
                             @endforeach
                            </ul>
                        </th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

