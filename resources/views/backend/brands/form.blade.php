
            <div class="form-row">
            <div class="col-md-12 mb-3">
                {{Form::label('name','Brands')}}
                {{Form::text('name', null, [
                      'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control',
                      'placeholder'=>'Enter The Size name',
                      'id'=>'name',
                ])}}
                @error('name')
                <div class="text-danger">{{ $message }}</div>
                @enderror

                </div>
            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('image','Image')}}<br>
                    {{Form::file('image', null, [
                          'class'=> $errors->has('image') ?  'form-control is-invalid': 'form-control',

                          'id'=>'image',
                    ])}}
                    @error('image')
                    <div class="text-danger">{{ $message }}</div>
                    @enderror

                </div>

            </div>

            <div class="form-row">
                <div class="col-md-12 mb-3">
                    {{Form::label('is_active','Active')}}

                    {{Form::radio('is_active','1')}}

                    {{Form::label('is_active','Inactive')}}

                    {{Form::radio('is_active','0')}}

                </div>
            </div>





