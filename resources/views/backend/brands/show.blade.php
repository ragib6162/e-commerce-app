@extends('backend.layouts.master')

@section('title', 'Brands Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show Brand</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('brands.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Brand</th>
                        <th>{{ $brand->name }}</th>
                    </tr>
                    <tr>
                        <th>Status</th>
                        <th>{{ $brand->is_active ? 'Active' : 'Inactive' }}</th>
                    </tr>
                    <tr>
                        <th>CreatedBy</th>
                        <th>{{ $brand->createdBy->name??null }}</th>
                    </tr>
                    <tr>
                        <th>UpdatedBy</th>
                        <th>{{ $brand->updatedBy->name??null }}</th>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

