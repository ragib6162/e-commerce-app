@extends('backend.layouts.master')

@section('title', 'Blogs Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show Tag</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('blogs.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Blogs</th>
                        <th>{{ $blog->title }}</th>
                    </tr>
                    <tr>
                        <th>CreatedBy</th>
                        <th>{{ $blog->createdBy->name??null }}</th>
                    </tr>
                    <tr>
                        <th>UpdatedBy</th>
                        <th>{{ $blog->updatedBy->name??null }}</th>
                    </tr>
                    <tr>
                        <th>Description</th>
                        <th>{{ $blog->description }}</th>
                    </tr>
                    <tr>
                        <th>Tags</th>
                        <th>
                            <ul>
                            @foreach($blog->tags as $tag)
                              <li>{{$tag->name}}</li>
                             @endforeach
                            </ul>
                        </th>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

