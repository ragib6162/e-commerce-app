


<div class="form-row">
<div class="col-md-12 mb-3">
    {{Form::label('title','Title')}}
    {{Form::text('title', null, [
          'class'=> $errors->has('title') ?  'form-control is-invalid': 'form-control',
          'placeholder'=>'Enter The title',
          'id'=>'title',
    ])}}
    @error('title')
    <div class="text-danger">{{ $message }}</div>
    @enderror

    </div>
</div>

{{--<div class="form-row">--}}
{{--    <div class="col-md-12 mb-3">--}}

{{--        {{Form::label('created_by','CreatedBy')}}<br>--}}
{{--        {{Form::select('created_by',$users, null,--}}
{{--               ["placeholder"=>"Select User"])}}--}}

{{--        @error('created_by')--}}
{{--        <div class="text-danger">{{ $message }}</div>--}}
{{--        @enderror--}}
{{--    </div>--}}
{{--</div>--}}
{{--@error('created_by')--}}
{{--<div class="text-danger">{{ $message }}</div>--}}
{{--@enderror--}}



{{--<div class="form-row">--}}
{{--<div class="col-md-12 mb-3">--}}

{{--    {{Form::label('updated_by','UpdatedBy')}}<br>--}}
{{--    {{Form::select('updated_by',$users, null,--}}
{{--           ["placeholder"=>"Select User"])}}--}}

{{--    @error('updated_by')--}}
{{--    <div class="text-danger">{{ $message }}</div>--}}
{{--    @enderror--}}

{{--</div>--}}

{{--</div>--}}


<div class="form-row">
    <div class="col-md-12 mb-3">
        {{Form::label('description','Description')}}
        {{Form::textarea('description', null, [
                          'class'=>$errors->has('description') ?  'form-control is-invalid' : 'form-control',
                          'placeholder'=>'Enter The Description',
                          'rows'=>'2',
                          'id'=>'description',
                    ])}}
        @error('description')
        <div class="text-danger">{{ $message }}</div>
        @enderror
    </div>

</div>
<div class="form-row">
    <div class="col-md-12 mb-3">
        {{ Form::label('tag', 'Tags') }}
        @foreach($tags as $key=>$tag)
            {!! Form::checkbox('tag[]', $key, in_array($key,$selectedTags)) !!}{{ $tag }}
        @endforeach
    </div>
</div>
