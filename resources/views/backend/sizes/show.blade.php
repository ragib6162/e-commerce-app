@extends('backend.layouts.master')

@section('title', 'Sizes Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Show Size</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('sizes.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                    </div>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th>Size</th>
                        <th>{{ $size->name }}</th>
                    </tr>
{{--                    <tr>--}}
{{--                        <th>CreatedBy</th>--}}
{{--                        <th>{{ $color->createdBy->name??null }}</th>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <th>UpdatedBy</th>--}}
{{--                        <th>{{ $color->updatedBy->name??null }}</th>--}}
{{--                    </tr>--}}

                    </tbody>
                </table>
            </div>
        </div>

    </div>
@endsection

