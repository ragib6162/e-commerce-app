
<div class="form-row">
<div class="col-md-12 mb-3">
    {{Form::label('name','Sizes')}}
    {{Form::text('name', null, [
          'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control',
          'placeholder'=>'Enter The Size name',
          'id'=>'name',
    ])}}
    @error('name')
    <div class="text-danger">{{ $message }}</div>
    @enderror

    </div>
</div>



