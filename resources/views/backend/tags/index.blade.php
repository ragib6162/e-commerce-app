@extends('backend.layouts.master')

@section('title', 'Tags')

@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-6">Tags</div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('tags.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>

                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
{{--                id="dataTable"--}}
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#SL</th>
                        <th>Tags</th>

                        <th>CreatedBy</th>
                        <th>UpdatedBy</th>
                        <th style="width: 150px; text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(session()->has('status'))
                     <div class="alert alert-success">

                            <p>{{session('status')}}</p>

                     </div>
                    @endif

                    @foreach($tags as $tag)
                    <tr>
                        <td>{{++$sl}}</td>

                        <td>{{$tag->name}}</td>

                        <td>{{$tag->createdBy->name??null}}</td>
                        <td>{{$tag->updatedBy->name??null}}</td>
                        <td>
                            <a href="{{ route('tags.show', $tag->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                            <a href="{{ route('tags.edit', $tag->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>
{{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}
                            <form action="{{route('tags.destroy', $tag->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button style="background-color: darkred;" type="submit" onclick="return confirm('Are you sure Want to delete?')">Delete</button>

                            </form>


                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            {{ $tags->links() }}
        </div>

    </div>

</div>
@endsection

@push('css')
    <!-- Custom styles for this page -->
    <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('script')
    <!-- Page level plugins -->
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
@endpush
