@extends('backend.layouts.master')

@section('title', 'Sub-Category Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-6">Show Sub Category</div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ route('sub-categories.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                                </div>
                            </div>
                        </div>

                 <div class="card-body">
                                    <div class="p-3">
                                    <h6> Sub-Category Image : </h6>
                                        @if(file_exists(storage_path().'/app/public/sub-categories/'.$subCategory->image) && (!is_null($subCategory->image)))
                                            <img src="{{ asset('storage/sub-categories/'.$subCategory->image) }}" height="100">
                                        @else
                                            {{--                                <img src="{{ asset('/default-avatar.jpg') }}">--}}
                                        @endif
                                    </div>



                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th>Title</th>
                                            <th>{{ $subCategory->title }}</th>
                                        </tr>
                                        <tr>
                                            <th>CreatedBy</th>
                                            <th>{{ $subCategory->createdBy->name??null }}</th>
                                        </tr>
                                        <tr>
                                            <th>UpdatedBy</th>
                                            <th>{{ $subCategory->updatedBy->name??null }}</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>{{ $subCategory->description }}</th>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <th>{{ $subCategory->is_active ? 'Active' : 'Inactive' }}</th>
                                        </tr>
                                        <tr>
                                            <th>Category</th>
                                            <th>
                                                {{$subCategory->category->title}}

                                            </th>
                                        </tr>

                                        </tbody>
                                    </table>



                        </div>
                    </div>

                </div>
@endsection

