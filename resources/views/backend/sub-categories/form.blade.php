

                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                        {{--                        <label for="product_title">Product_Title</label>--}}
                                                {{Form::label('title','Title')}}
                        {{--                        <input type="text" name="title" class="form-control " placeholder="Enter The Title" >--}}
                                                {{Form::text('title', null, [
                                                      'class'=> $errors->has('title') ?  'form-control is-invalid': 'form-control',
                                                      'placeholder'=>'Enter The title',
                                                      'id'=>'title',
                                                ])}}
                                                @error('title')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror

                                            </div>

                                        </div>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            {{Form::label('image','Image')}}<br>
                                            {{Form::file('image', null, [
                                                  'class'=> $errors->has('image') ?  'form-control is-invalid': 'form-control',

                                                  'id'=>'image',
                                            ])}}
                                            @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror

                                        </div>

                                    </div>




                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-3">

                                                            {{Form::label('is_active','Active')}}
                                                            {{Form::radio('is_active','1')}}
                                                            {{Form::label('is_active','Inactive')}}
                                                            {{Form::radio('is_active','0')}}

                                                </div>
                                          </div>

                                <div class="form-row">
                                    <div class="col-md-12 mb-3">

                                             {{Form::label('category_id','Category')}}<br>
                                             {{Form::select('category_id',$categories, null,
                                                    ["placeholder"=>"Select One"])}}

                                    </div>
                                </div>


                                <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{--        <label for="description">Description</label>--}}
                                {{Form::label('description','Description')}}
                                {{--        <textarea type="text" name="description" class="form-control " placeholder="Enter The Description" >--}}
                                {{--                        </textarea>--}}
                                {{Form::textarea('description', null, [
                                                  'class'=>$errors->has('description') ?  'form-control is-invalid' : 'form-control',
                                                  'placeholder'=>'Enter The Description',
                                                  'rows'=>'2',
                                                  'id'=>'description',
                                            ])}}
                                @error('description')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror

                            </div>

                        </div>

{{--                            <div class="form-row">--}}
{{--                                <div class="col-md-12 mb-3">--}}
{{--                                    {{ Form::label('color', 'Colors') }}--}}
{{--                                    @foreach($colors as $key=>$color)--}}
{{--                                        {!! Form::checkbox('color[]', $key, in_array($key, $selectedColors)) !!}{{ $color }}--}}
{{--                                    @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}

{{--                            <div class="form-row">--}}
{{--                                <div class="col-md-12 mb-3">--}}
{{--                                    {{ Form::label('tag', 'Tags') }}--}}
{{--                                    @foreach($tags as $key=>$tag)--}}
{{--                                        {!! Form::checkbox('tag[]', $key, in_array($key,$selectedTags)) !!}{{ $tag }}--}}
{{--                                    @endforeach--}}
{{--                                </div>--}}
{{--                            </div>--}}
