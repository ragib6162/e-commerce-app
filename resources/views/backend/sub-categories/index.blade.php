@extends('backend.layouts.master')

@section('title', 'Sub-Categories')

@section('content')
<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <div class="row">
                <div class="col-md-6">'Sub-Categories</div>
                <div class="col-md-6 text-right">
                    <a href="{{ route('sub-categories.create') }}" class="btn btn-sm btn-outline-primary">Add New</a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
{{--                id="dataTable"--}}
                <table class="table table-bordered"  width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>#SL</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Status</th>
                        <th>Category</th>
                        <th>CreatedBy</th>
                        <th>UpdatedBy</th>
                        <th style="width: 150px; text-align: center;">Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if(session()->has('status'))
                     <div class="alert alert-success">

                            <p>{{session('status')}}</p>

                     </div>
                    @endif

                    @foreach($subCategories as $subCategory)
                    <tr>
                        <td>{{++$sl}}</td>
                        <td>
                            @if(file_exists(storage_path().'/app/public/sub-categories/'.$subCategory->image) && (!is_null($subCategory->image)))
                                <img src="{{ asset('storage/sub-categories/'.$subCategory->image) }}" height="100">
                            @else
{{--                                <img src="{{ asset('/default-avatar.jpg') }}">--}}
                            @endif
                        </td>
                        <td>{{$subCategory->title}}</td>
                        <td>{{$subCategory->is_active ? 'Active' : 'Inactive'}}</td>
                        <td>
                            {{$subCategory->category->title}}
{{--                            @if($product->category_id == 1)--}}
{{--                                Men's Category--}}
{{--                             @elseif($product->category_id == 2)--}}
{{--                                Women's Category--}}
{{--                            @elseif($product->category_id == 3)--}}
{{--                                Kid's Category--}}
{{--                             @endif--}}

                        </td>
                        <td>{{$subCategory->createdBy->name??null}}</td>

                        <td>{{$subCategory->updatedBy->name??null}}</td>

                        <td>
                            <a href="{{ route('sub-categories.show', $subCategory->id) }}" class="btn btn-sm btn-outline-info">Show</a>
                            <a href="{{ route('sub-categories.edit', $subCategory->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>
{{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}
                            <form action="{{route('sub-categories.destroy', $subCategory->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button style="background-color: darkred;" type="submit" onclick="return confirm('Are you sure Want to delete?')">Delete</button>

                            </form>


                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
{{--            {{ $subCategories->links() }}--}}
        </div>
{{--        <div class="card-footer text-right">--}}
{{--            <a href="{{ route('products.excel') }}" class="btn btn-sm btn-outline-danger">Download Excel</a>--}}
{{--            <a href="{{ route('products.pdf') }}" class="btn btn-sm btn-outline-danger">Download Pdf</a>--}}
{{--        </div>--}}
    </div>

</div>
@endsection

@push('css')
    <!-- Custom styles for this page -->
    <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('script')
    <!-- Page level plugins -->
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
@endpush
