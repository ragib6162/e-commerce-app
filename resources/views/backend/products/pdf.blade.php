<h1>Products List</h1>

<table border="1px solid">
    <thead>
    <tr>
        <th>#SL</th>
        <th>Product_Title</th>
        <th>Status</th>
        <th>Category_id</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody>
    @foreach($products as $product)
    <tr>
        <td>{{$loop->iteration}}</td>
        <td>{{$product->product_title}}</td>
        <td>{{$product->is_active ? 'Active' : 'Inactive'}}</td>
        <td>
            @if($product->category_id == 1)
                Men's Category
            @elseif($product->category_id == 2)
                Women's Category
            @elseif($product->category_id == 3)
                Kid's Category
            @endif
        </td>
        <td>{{$product->description}}</td>
    </tr>
    @endforeach
    </tbody>


</table>
