@extends('backend.layouts.master')

@section('title', 'Trash_Products')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Products (Trash)</div>
                    <div class="col-md-6 text-right">
                        <a href="{{ route('products.index') }}" class="btn btn-sm btn-outline-primary">List</a>

                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    {{--                id="dataTable"--}}
                    <table class="table table-bordered"  width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>#SL</th>
                            <th>Product_Title</th>
                            <th>Status</th>
                            <th>Category_id</th>
                            <th style="width: 150px; text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if(session()->has('status'))
                            <div class="alert alert-success">

                                <p>{{session('status')}}</p>

                            </div>
                        @endif

                        @foreach($products as $product)
                            <tr>
                                <td>{{++$sl}}</td>
                                <td>{{$product->product_title}}</td>
                                <td>{{$product->is_active ? 'Active' : 'Inactive'}}</td>
                                <td>
                                    @if($product->category_id == 1)
                                        Men's Category
                                    @elseif($product->category_id == 2)
                                        Women's Category
                                    @elseif($product->category_id == 3)
                                        Kid's Category
                                    @endif
                                </td>
                                <td>
                                    <a href="{{ route('products.trash_show', $product->id) }}" class="btn btn-sm btn-outline-info">Show</a>

                                    {{Form::open([
                                      'route'=>['products.trash_restore', $product->id],
                                      'method'=>'PUT',
                                      'style' => 'display:inline',
                                     ])}}
                                            <button style="background-color: dodgerblue;" type="submit" onclick="return confirm('Are you sure Want to Restore?')">Restore</button>
                                    {{Form::close()}}

{{--                                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-sm btn-outline-warning">Edit</a>--}}
                                    {{--                            <a href="#" class="btn btn-sm btn-outline-danger">Delete</a>--}}
{{--                                    <form action="{{route('products.destroy', $product->id)}}" method="post">--}}
{{--                                        @csrf--}}
{{--                                        @method('DELETE')--}}
{{--                                        <button type="submit" onclick="return confirm('Are you sure Want to delete?')">Delete</button>--}}

{{--                                    </form>--}}

                                    {{Form::open([
                                      'route'=>['products.trash_delete', $product->id],
                                      'method'=>'DELETE',

                                     ])}}
                                             <button style="background-color: red;" type="submit" onclick="return confirm('Are you sure Want to delete permanently?')">Delete</button>
                                    {{Form::close()}}


                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $products->links() }}
            </div>
        </div>

    </div>
@endsection

@push('css')
    <!-- Custom styles for this page -->
    <link href="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush

@push('script')
    <!-- Page level plugins -->
    <script src="{{ asset('ui/backend') }}/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="{{ asset('ui/backend') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="{{ asset('ui/backend') }}/js/demo/datatables-demo.js"></script>
@endpush

