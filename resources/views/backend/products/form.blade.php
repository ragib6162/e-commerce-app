

                        <div class="form-row">
                                            <div class="col-md-12 mb-3">
                                                {{Form::label('product_title','Product_Title')}}
                                                {{Form::text('product_title', null, [
                                                      'class'=> $errors->has('product_title') ?  'form-control is-invalid': 'form-control',
                                                      'placeholder'=>'Enter The Product_Title',
                                                      'id'=>'product_title',
                                                ])}}
                                                @error('product_title')
                                                <div class="text-danger">{{ $message }}</div>
                                                @enderror

                                            </div>

                                        </div>
                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            {{Form::label('image','Image')}}<br>
                                            {{Form::file('image', null, [
                                                  'class'=> $errors->has('image') ?  'form-control is-invalid': 'form-control',

                                                  'id'=>'image',
                                            ])}}
                                            @error('image')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror

                                        </div>

                                    </div>


                                    <div class="form-row">
                                        <div class="col-md-12 mb-3">
                                            {{ Form::label('picture', 'Multiple Product Image') }}<br>
                                            {!! Form::file('picture[]',
                                            ['multiple'=>true,'class'=>'form-control']) !!}
                                            @error('picture')
                                            <div class="text-danger">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                                    <div class="form-row">
                                                        <div class="col-md-12 mb-3">
                                                            {{Form::label('is_active','Active')}}

                                                            {{Form::radio('is_active','1')}}

                                                            {{Form::label('is_active','Inactive')}}

                                                            {{Form::radio('is_active','0')}}

                                                </div>
                                          </div>

                                <div class="form-row">
                                    <div class="col-md-12 mb-3">
                                             {{Form::label('category_id','Category')}}<br>
                                             {{Form::select('category_id',$categories, null,
                                                    ["placeholder"=>"Select One"])}}
                                        @error('category_id')
                                        <div class="text-danger">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{Form::label('sub_category_id','Sub-Category')}}<br>
                                {{Form::select('sub_category_id',$subCategories, null,
                                       ["placeholder"=>"Select One"])}}
                                @error('sub_category_id')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                                <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{Form::label('description','Description')}}

                                {{Form::textarea('description', null, [
                                                  'class'=>$errors->has('description') ?  'form-control is-invalid' : 'form-control',
                                                  'placeholder'=>'Enter The Description',
                                                  'rows'=>'2',
                                                  'id'=>'description',
                                            ])}}
                                @error('description')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror

                            </div>

                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{ Form::label('size', 'Sizes') }}
                                @foreach($sizes as $key=>$size)
                                    {!! Form::checkbox('size[]', $key, in_array($key,$selectedSizes)) !!}{{ $size }}
                                @endforeach
                            </div>
                        </div>


                        <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    {{ Form::label('color', 'Colors') }}
                                    @foreach($colors as $key=>$color)
                                        {!! Form::checkbox('color[]', $key, in_array($key, $selectedColors)) !!}{{ $color }}
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{ Form::label('tag', 'Tags') }}
                                @foreach($tags as $key=>$tag)
                                    {!! Form::checkbox('tag[]', $key, in_array($key,$selectedTags)) !!}{{ $tag }}
                                @endforeach
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{ Form::label('brand', 'Brands') }}
                                @foreach($brands as $key=>$brand)
                                    {!! Form::checkbox('brand[]', $key, in_array($key,$selectedBrands)) !!}{{ $brand }}
                                @endforeach
                            </div>
                        </div>
