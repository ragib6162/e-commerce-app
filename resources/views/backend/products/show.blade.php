@extends('backend.layouts.master')

@section('title', 'Products Details')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <div class="row">
                                <div class="col-md-6">Show Product</div>
                                <div class="col-md-6 text-right">
                                    <a href="{{ route('products.index') }}" class="btn btn-sm btn-outline-primary">List</a>
                                </div>
                            </div>
                        </div>

                 <div class="card-body">
                                    <div class="p-3">
                                    <h6> Product Image : </h6>
                                    @if(file_exists(storage_path().'/app/public/products/'.$product->image) && (!is_null($product->image)))
                                        <img src="{{ asset('storage/products/'.$product->image) }}" height="100">
                                    @else
                    {{--                    <img src="{{ asset('/default-avatar.jpg') }}">--}}
                                    @endif
                                    </div>


                <div class="p-3">
                    <h6> Product Multiple Images : </h6>
                    @foreach($product->pictures as $img)
                        @if(file_exists(storage_path().'/app/public/products/'.$img->picture ) && (!is_null($img->picture)))
                            <img src="{{ asset('storage/products/'.$img->picture) }}" height="100">
                        @endif
                    @endforeach
                </div>
                                    <table class="table table-striped">
                                        <tbody>
                                        <tr>
                                            <th>Product_Title</th>
                                            <th>{{ $product->product_title }}</th>
                                        </tr>
                                        <tr>
                                            <th>CreatedBy</th>
                                            <th>{{ $product->createdBy->name??null }}</th>
                                        </tr>
                                        <tr>
                                            <th>UpdatedBy</th>
                                            <th>{{ $product->updatedBy->name??null }}</th>
                                        </tr>
                                        <tr>
                                            <th>Description</th>
                                            <th>{{ $product->description }}</th>
                                        </tr>
                                        <tr>
                                            <th>Status</th>
                                            <th>{{ $product->is_active ? 'Active' : 'Inactive' }}</th>
                                        </tr>
                                        <tr>
                                            <th>Category</th>
                                            <th>
                                                {{$product->category->title}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Sub-Category</th>
                                            <th>
                                                {{$product->subCategory->title}}
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Sizes</th>
                                            <th>
                                                <ul>
                                                    @foreach($product->sizes as $size)
                                                        <li>{{$size->name}}</li>
                                                    @endforeach
                                                </ul>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Colors</th>
                                            <th>
                                                <ul>
                                                    @foreach($product->colors as $color)
                                                        <li>{{$color->name}}</li>
                                                    @endforeach
                                                </ul>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Tags</th>
                                            <th>
                                                <ul>
                                                    @foreach($product->tags as $tag)
                                                        <li>{{$tag->name}}</li>
                                                    @endforeach
                                                </ul>
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>Brands</th>
                                            <th>
                                                <ul>
                                                    @foreach($product->brands as $brand)
                                                        <li>{{$brand->name}}</li>
                                                    @endforeach
                                                </ul>
                                            </th>
                                        </tr>

                                        </tbody>
                                    </table>
                                        <tr>
                                            <th>Comments</th>
                                            <th>
                                                <ol>
                                                    @foreach($product->comments as $comment)
                                                        <li>
                                                            {{$comment->body}}
                                                            Commented At <mark>{{$comment->created_at->toFormattedDateString()}}  {{$comment->created_at->diffForHumans()}}</mark>
                                                            By <mark>{{$comment->commentedBy->name}}</mark>
                                                        </li>
                                                    @endforeach
                                                </ol>
                                            </th>
                                        </tr>

                    {{ Form::open(['route'=>['products.comment', $product->id],
                             ])}}

                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            {{Form::label('body','Write Your Comment.....')}}
                            {{Form::textarea('body', null, [
                                              'class'=>$errors->has('body') ?  'form-control is-invalid' : 'form-control',
                                              'id'=>'body',
                                        ])}}

                        </div>
                        @error('body')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    {{Form::button('Post Comment',[
                                     'class'=>'btn btn-primary',
                                     'type'=>'submit',
                                        ])}}

                    {{--            </form>--}}
                    {{ Form::close()}}

                        </div>
                    </div>

                </div>
@endsection

