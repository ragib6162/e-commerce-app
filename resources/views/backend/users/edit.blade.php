@extends('backend.layouts.master')

@section('title', 'Profile Edit')

@section('content')
    <div class="container-fluid">

        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <div class="row">
                    <div class="col-md-6">Edit Profile</div>
                    <div class="col-md-6 text-right">
{{--                        <a href="{{ route('products.index') }}" class="btn btn-sm btn-outline-primary">List</a>--}}
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{Form::model($user, [
                      'route'=>['users.update',
                      $user->id],
                      'method'=>'put',
                      'files'=>true,
                 ])}}

                @csrf
                @method('PUT')

                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            {{Form::label('name','Name')}}
                            {{Form::text('name', null, [
                                  'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control',
                                  'placeholder'=>'Enter The Name',
                                  'id'=>'name',
                            ])}}
                            @error('name')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror

                        </div>

                    </div>

                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            {{Form::label('picture','Picture')}}<br>
                            {{Form::file('picture', null, [
                                  'class'=> $errors->has('picture') ?  'form-control is-invalid': 'form-control',

                                  'id'=>'picture',
                            ])}}
                            @error('picture')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror

                        </div>

                    </div>
                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            {{Form::label('facebook_url','Facebook')}}
                            {{Form::text('facebook_url', $user->profile->facebook_url, [
                                  'class'=> $errors->has('facebook_url') ?  'form-control is-invalid': 'form-control',

                                  'id'=>'facebook_url',
                            ])}}
                            @error('facebook_url')
                            <div class="text-danger">{{ $message }}</div>
                            @enderror

                        </div>
                    </div>
                        <div class="form-row">
                            <div class="col-md-12 mb-3">
                                {{Form::label('twitter_url','Twitter')}}
                                {{Form::text('twitter_url', $user->profile->twitter_url, [
                                      'class'=> $errors->has('twitter_url') ?  'form-control is-invalid': 'form-control',

                                      'id'=>'twitter_url',
                                ])}}
                                @error('twitter_url')
                                <div class="text-danger">{{ $message }}</div>
                                @enderror

                            </div>
                        </div>
                            <div class="form-row">
                                <div class="col-md-12 mb-3">
                                    {{Form::label('linked_in_url','LinkID')}}
                                    {{Form::text('linked_in_url', $user->profile->linked_in_url, [
                                          'class'=> $errors->has('linked_in_url') ?  'form-control is-invalid': 'form-control',

                                          'id'=>'linked_in_url',
                                    ])}}
                                    @error('linked_in_url')
                                    <div class="text-danger">{{ $message }}</div>
                                    @enderror

                                </div>

                    </div>


                    <div class="form-row">
                        <div class="col-md-12 mb-3">
                            {{Form::label('bio','Bio')}}
                            {{Form::textarea('bio', $user->profile->bio, [
                                              'class'=>$errors->has('bio') ?  'form-control is-invalid' : 'form-control',
                                              'rows'=>'2',
                                              'id'=>'bio',
                                        ])}}

                        </div>
                        @error('bio')
                        <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>


                    {{--                    <button class="btn btn-primary" type="submit">Submit form</button>--}}
                {{Form::button('Submit form',[
                                'class'=>'btn btn-primary',
                                'type'=>'submit',
                                   ])}}
                {{--                </form>--}}
                {{ Form::close()}}
            </div>
        </div>
    </div>
@endsection

