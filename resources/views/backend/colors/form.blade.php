




{{--<div class="form-row">--}}
{{--    <div class="col-md-12 mb-3">--}}

{{--        {{Form::label('created_by','CreatedBy')}}<br>--}}
{{--        {{Form::select('created_by',$users, null,--}}
{{--               ["placeholder"=>"Select User"])}}--}}

{{--    </div>--}}
{{--</div>--}}

{{--<div class="form-row">--}}
{{--    <div class="col-md-12 mb-3">--}}

{{--        {{Form::label('updated_by','UpdatedBy')}}<br>--}}
{{--        {{Form::select('updated_by',$users, null,--}}
{{--               ["placeholder"=>"Select User"])}}--}}

{{--    </div>--}}
{{--</div>--}}



<div class="form-row">
<div class="col-md-12 mb-3">
    {{Form::label('name','Colors')}}
    {{Form::text('name', null, [
          'class'=> $errors->has('name') ?  'form-control is-invalid': 'form-control',
          'placeholder'=>'Enter The Color name',
          'id'=>'name',
    ])}}
    @error('name')
    <div class="text-danger">{{ $message }}</div>
    @enderror

    </div>
</div>

{{--<div class="form-row">--}}
{{--    <div class="col-md-12 mb-3">--}}

{{--        {{Form::label('created_by','CreatedBy')}}<br>--}}
{{--        {{Form::select('created_by',$users, null,--}}
{{--               ["placeholder"=>"Select User"])}}--}}

{{--    </div>--}}
{{--</div>--}}
{{--@error('created_by')--}}
{{--<div class="text-danger">{{ $message }}</div>--}}
{{--@enderror--}}



{{--<div class="form-row">--}}
{{--<div class="col-md-12 mb-3">--}}

{{--    {{Form::label('updated_by','UpdatedBy')}}<br>--}}
{{--    {{Form::select('updated_by',$users, null,--}}
{{--           ["placeholder"=>"Select User"])}}--}}

{{--</div>--}}
{{--</div>--}}
{{--@error('updated_by')--}}
{{--<div class="text-danger">{{ $message }}</div>--}}
{{--@enderror--}}


