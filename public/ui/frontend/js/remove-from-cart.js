
function removeFromCart(id) {




    // console.log($('meta[name="csrf-token"]').attr('content'));


    $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        type: "post",
        url: APP_URL+'remove_From_cart',
        data: {productId: id},
        cache: false,
        success: function (res) {


            $('.rem'+id).empty();
            console.log(res)
        },

        error: function (xhr, status, error) {
            console.log("An AJAX error occured: " + status + "\nError: " + error);
        }
    });


}
